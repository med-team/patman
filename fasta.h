// PatMaN DNA pattern matcher
// (C) 2007 Kay Pruefer, Udo Stenzel
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or (at
// your option) any later version.  See the LICENSE file for details.

#ifndef INCLUDED_FASTA_H
#define INCLUDED_FASTA_H

#include <string>
#include <iostream>

typedef struct fasta {
	std::string seq ;
	std::string headerline ;
	int null ;
} fasta ;

class fasta_fac {
	public:
		fasta_fac( std::istream& i ) : in(i), line(0) {  } 
		fasta* get(  ) ;
	private: 
		std::istream& in ;
		int line ;
} ;

#endif
